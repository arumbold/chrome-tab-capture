// snapshot timer
var active = false;
var sender;

// snap every 0.9 Seconds
// sender = window.setInterval(sendSnapshot, 900);

// send update when tab changes
chrome.tabs.onActivated.addListener(tabEvent);
chrome.tabs.onUpdated.addListener(tabEvent);


// listeners
function tabEvent(info){
	if (info.windowId){
		chrome.tabs.captureVisibleTab(info.windowId, sendSnapshot);
	} else {
		chrome.tabs.captureVisibleTab(sendSnapshot);
	}
}

function sendSnapshot(img)
{
	if (chrome.runtime.lastError)
	{
		console.log(chrome.runtime.lastError.message);
	}

	if (localStorage.transmitDestination && img){
		transmit({ tabImg: img});
	}
}

// transmit
function transmit(msg){
	console.log("snapshot");
	chrome.runtime.sendMessage(localStorage.transmitDestination,msg);
}
