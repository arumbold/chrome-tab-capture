function save_options(){
	var destination = document.getElementById("destination").value
	if (destination) localStorage.transmitDestination = destination;
}

function load_options(){
	if (localStorage.transmitDestination)
	{
		document.getElementById("destination").value = localStorage.transmitDestination;
	}
}

function setup(){
	load_options();
	document.getElementById('save').addEventListener('click', save_options);
}

document.addEventListener('DOMContentLoaded', setup);
