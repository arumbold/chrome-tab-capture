# Chrome Tab Capture
Chrome Tab Capture is a chrome extension that sends an image of the visible area of your currently selected tab every 900ms,
or whenever the selected tab changes, to a listening chrome application or extension.

The Destination can be set from within the options page. 
